<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'isVerified', 'owner', 'legality', 'year_establish', 'category_id', 'category_level_1', 'category_level_2', 'category_level_3', 'category_level_4', 'category_level_5', 'company_district', 'company_address', 'company_postal', 'branch_district', 'branch_address', 'branch_postal', 'contact_email', 'phone', 'website', 'social_media'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function CategoryLevel1()
    {
        return $this->belongsTo('App\CategoryLevel', 'category_level_1');
    }

    public function CategoryLevel2()
    {
        return $this->belongsTo('App\CategoryLevel', 'category_level_2');
    }

    public function CategoryLevel3()
    {
        return $this->belongsTo('App\CategoryLevel', 'category_level_3');
    }

    public function CategoryLevel4()
    {
        return $this->belongsTo('App\CategoryLevel', 'category_level_4');
    }

    public function CategoryLevel5()
    {
        return $this->belongsTo('App\CategoryLevel', 'category_level_5');
    }

    public function Data()
    {
        return $this->hasMany('App\Data');
    }

    public function Attendance()
    {
        return $this->hasMany('App\Attendance');
    }

    public function District()
    {
        return $this->belongsTo('App\District', 'company_district');
    }
}
