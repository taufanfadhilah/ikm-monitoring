<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marketing extends Model
{
    protected $fillable = ['data_id', 'category', 'location', 'value', 'description'];
}
