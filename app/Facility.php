<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facility extends Model
{
    use SoftDeletes;
    protected $fillable = ['facility_category_id', 'name', 'instance', 'start_date', 'end_date'];

    public function FacilityCategory()
    {
        return $this->belongsTo('App\FacilityCategory');
    }

    public function Attendance()
    {
        return $this->hasMany('App\Attendance');
    }
}
