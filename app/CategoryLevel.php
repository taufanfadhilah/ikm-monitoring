<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryLevel extends Model
{
    protected $fillable = ['category', 'sub_category', 'group', 'sub_group', 'detail_group', 'title'];

    public $timestamps = false;
}
