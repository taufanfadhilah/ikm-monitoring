<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $fillable = ['data_id', 'origin', 'kind', 'quality'];
}
