<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attendance extends Model
{
    use SoftDeletes;
    protected $fillable = ['facility_id', 'user_id', 'product_id'];

    public function User()
    {
        return $this->belongsTo('App\User');
    }

    public function Facility()
    {
        return $this->belongsTo('App\Facility');
    }

    public function Product()
    {
        return $this->belongsTo('App\Product');
    }
}
