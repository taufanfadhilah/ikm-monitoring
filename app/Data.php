<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Data extends Model
{
    use SoftDeletes;
    protected $table = 'datas';
    protected $fillable = ['user_id', 'facility_id', 'year', 'investment', 'wni_male', 'wni_female', 'wna_male', 'wna_female', 'labor_total', 'industrial_permit_number', 'industrial_permit_period', 'siup_number', 'siup_period', 'tdp_number', 'tdp_period', 'nib_number', 'nib_period', 'halal_number', 'halal_period', 'sni_number', 'sni_period', 'iso_number', 'iso_period', 'pirt_number', 'pirt_period'];

    public function Facility()
    {
        return $this->belongsTo('App\Facility');
    }

    public function Product()
    {
        return $this->hasMany('App\Product');
    }

    public function Material()
    {
        return $this->hasMany('App\Material');
    }

    public function Marketing()
    {
        return $this->hasMany('App\Marketing');
    }
}
