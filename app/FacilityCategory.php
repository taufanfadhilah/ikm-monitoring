<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacilityCategory extends Model
{
    use SoftDeletes;
    protected $fillable = ['name'];

    public function Facility()
    {
        return $this->hasMany('App\Facility');
    }
}
