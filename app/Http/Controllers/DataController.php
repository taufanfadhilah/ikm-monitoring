<?php

namespace App\Http\Controllers;

use App\Data;
use Illuminate\Http\Request;
use App\Facility;
use App\Marketing;
use App\Material;
use App\Product;
use Storage;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $facilities = Facility::all();
        return view('data.create', [
            'facilities' => $facilities
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Data::create($request->all());

        for ($i=0; $i < count($request->category); $i++) { 
            Marketing::create([
                'data_id' => $data->id,
                'category' => $request->category[$i],
                'location' => $request->location[$i],
                'value' => $request->value[$i],
                'description' => $request->description[$i],
            ]);
        }

        for ($i=0; $i < count($request->origin) ; $i++) { 
            Material::create([
                'data_id' => $data->id,
                'origin' => $request->origin[$i],
                'kind' => $request->kind[$i],
                'quanity' => $request->quanity[$i]
            ]);
        }

        for ($i=0; $i < count($request->name); $i++) { 
            $photo = '';
            if ($request->photo[$i]) {
                $photo = Storage::disk('public')->put('products', $request->photo[$i]);
            }
            Product::create([
                'data_id' => $data->id,
                'name' => $request->name[$i],
                'photo' => $photo,
                'production_capacity' => $request->production_capacity[$i],
                'production_value' => $request->production_value[$i]
            ]);
        }
        $request->session()->flash('status', 'Data berhasil disimpan');
        return redirect(route('dashboard'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function show(Data $data)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function edit(Data $data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Data $data)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function destroy(Data $data)
    {
        //
    }
}
