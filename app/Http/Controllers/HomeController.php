<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryLevel;
use App\Data;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function validation()
    {
        if(!Auth::user()->isVerified){
            return view('notVerified');
        }
        return redirect(route('dashboard'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function dashboard()
    {
        return view('home');
    }

    public function dashboardYear(Request $request)
    {
        $data = Data::where('year', $request->year)->first();
        return view('home', [
            'data' => $data
        ]);
    }

    public function getLevel2($id)
    {
        $categoryLevels = CategoryLevel::where([
            ['category', '=', $id],
            ['sub_category', '!=', ''],
            ['group', '=', ''],
        ])->get();
        return response()->json($categoryLevels);
    }

    public function getLevel3($id)
    {
        $categoryLevels = CategoryLevel::where([
            ['sub_category', '=', $id],
            ['group', '!=', ''],
            ['sub_group', '=', ''],
        ])->get();
        return response()->json($categoryLevels);
    }

    public function getLevel4($id)
    {
        $categoryLevels = CategoryLevel::where([
            ['group', '=', $id],
            ['sub_group', '!=', ''],
            ['detail_group', '=', ''],
        ])->get();
        return response()->json($categoryLevels);
    }

    public function getLevel5($id)
    {
        $categoryLevels = CategoryLevel::where([
            ['sub_group', '=', $id],
            ['detail_group', '!=', '']
        ])->get();
        return response()->json($categoryLevels);
    }
}
