<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\FacilityCategory;
use App\District;

class SuperAdminController extends Controller
{
    public function index()
    {
        $districts = District::with('Attendance')->get();
        return view('superAdmin.index',[
            'districts' => $districts
        ]);
    }

    public function detailChart(District $district)
    {
        $facilities = $district->Attendance->groupBy('facility_id');
        return view('superAdmin.detailChart', [
            'facilities' => $facilities,
            'district' => $district
        ]);
    }

    public function adminBidang()
    {
        $users = User::withTrashed()->where('role', 'Admin Bidang')->get();
        return view('superAdmin.adminBidang.index', [
            'users' => $users
        ]);
    }

    public function adminBidangCreate()
    {
        return view('superAdmin.adminBidang.create');
    }

    public function adminBidangStore(Request $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => 'Admin Bidang',
            'isVerified' => 1
        ]);
        $request->session()->flash('status', 'Data berhasil disimpan');
        return redirect(route('superAdmin.adminBidang.index'));
    }

    public function adminBidangEdit(User $user)
    {
        return view('superAdmin.adminBidang.edit', [
            'user' => $user
        ]);
    }

    public function adminBidangUpdate(Request $request, User $user)
    {
        $user->name = $request->name;   
        $user->email = $request->email;
        if ($request->password) {
            $user->password = $request->password;
        }
        $user->save();
        $request->session()->flash('status', 'Data berhasil disimpan');
        return redirect(route('superAdmin.adminBidang.index'));
    }

    public function adminBidangDestroy(User $user)
    {
        $user->delete();
        return redirect(route('superAdmin.adminBidang.index'));
    }

    public function facilityCategory()
    {
        $categories = FacilityCategory::all();
        return view('superAdmin.facilityCategory.index', [
            'categories' => $categories
        ]);
    }

    public function facilityCategoryCreate()
    {
        return view('superAdmin.facilityCategory.create');
    }

    public function facilityCategoryStore(Request $request)
    {
        FacilityCategory::create($request->all());
        $request->session()->flash('status', 'Data berhasil disimpan');
        return redirect(route('superAdmin.facilityCategory.index'));
    }

    public function facilityCategoryEdit(FacilityCategory $facilityCategory)
    {
        return view('superAdmin.facilityCategory.edit', [
            'facilityCategory' => $facilityCategory
        ]);
    }

    public function facilityCategoryUpdate(Request $request, FacilityCategory $facilityCategory)
    {
        $facilityCategory->update($request->all());
        $request->session()->flash('status', 'Data berhasil disimpan');
        return redirect(route('superAdmin.facilityCategory.index'));
    }

    public function facilityCategoryDestroy(FacilityCategory $facilityCategory)
    {
        $facilityCategory->delete();
        return redirect(route('superAdmin.facilityCategory.index'));
    }
}
