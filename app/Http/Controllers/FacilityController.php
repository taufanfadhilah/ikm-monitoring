<?php

namespace App\Http\Controllers;

use App\Facility;
use Illuminate\Http\Request;
use App\FacilityCategory;
use App\Category;

class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facilities = Facility::all();
        return view('admin.facility.index', [
            'facilities' => $facilities
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = FacilityCategory::all();
        $instances = Category::all();
        return view('admin.facility.create', [
            'categories' => $categories,
            'instances' => $instances
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Facility::create($request->all());
        $request->session()->flash('status', 'Data berhasil disimpan');
        return redirect(route('facility.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function show(Facility $facility)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function edit(Facility $facility)
    {
        $categories = FacilityCategory::all();
        $instances = Category::all();
        return view('admin.facility.edit', [
            'facility' => $facility,
            'categories' => $categories,
            'instances' => $instances
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facility $facility)
    {
        $facility->update($request->all());
        $request->session()->flash('status', 'Data berhasil disimpan');
        return redirect(route('facility.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facility $facility)
    {
        $facility->delete();
        return redirect(route('facility.index'));
    }
}
