<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;
use App\CategoryLevel;
use App\District;
use App\Data;
use App\Marketing;
use App\Material;
use App\Product;
use App\Attendance;
use Storage;

class AdminBidangController extends Controller
{
    public function index()
    {
        $districts = District::with('Attendance')->get();
        return view('admin.index', [
            'districts' => $districts
        ]);
    }

    public function detailChart(District $district)
    {
        $facilities = $district->Attendance->groupBy('facility_id');
        return view('admin.detailChart', [
            'facilities' => $facilities,
            'district' => $district
        ]);
    }

    public function getApprove()
    {
        $users = User::where([
            ['isVerified', 0],
            ['role', 'IKM']
        ])->get();
        return view('admin.user.approve', [
            'users' => $users
        ]);
    }

    public function doApprove(Request $request)
    {
        $user = User::find($request->user_id);
        $user->isVerified = 1;
        $user->save();

        $request->session()->flash('status', 'IKM berhasil approve');
        return redirect(route('adminBidang.getApprove'));
    }

    public function getIKM()
    {
        $users = User::where([
            ['role', 'IKM'],
            ['isVerified', 1]
        ])->get();
        return view('admin.user.index', [
            'users' => $users
        ]);
    }

    public function detailIKM(User $user)
    {
        $categories = Category::all();
        $categoryLevels = CategoryLevel::where([
            ['sub_category', '=', '']
        ])->get();
        $districts = District::all();
        return view('admin.user.detail', [
            'user' => $user,
            'categories' => $categories,
            'categoryLevels' => $categoryLevels,
            'districts' => $districts
        ]);
    }

    public function updateIKM(Request $request, User $user)
    {
        $user->update($request->all());
        $request->session()->flash('status', 'Profil berhasil diperbaharui');
        return redirect(route('adminBidang.detailIKM', ['user' => $user->id]));
    }

    public function deleteIKM(Request $request, User $user)
    {
        $user->delete();
        $request->session()->flash('status', 'IKM berhasil dihapus');
        return redirect(route('adminBidang.getIKM'));
    }

    public function listData(User $user)
    {
        return view('admin.data.list', [
            'user' => $user,
            'datas' => $user->Data
        ]);
    }

    public function detailData(Data $data)
    {
        return view('admin.data.detail', [
            'data' => $data
        ]);
    }

    public function addData(User $user)
    {
        return view('admin.data.create',[
            'user' => $user
        ]);
    }

    public function storeData(Request $request)
    {
        // return $request;
        $data = Data::create($request->all());

        if (count($request->category) > 0) {
            for ($i=0; $i < count($request->category); $i++) { 
                Marketing::create([
                    'data_id' => $data->id,
                    'category' => $request->category[$i],
                    'location' => $request->location[$i],
                    'value' => $request->value[$i]
                ]);
            }
        }

        if(isset($request->origin)){
            for ($i=0; $i < count($request->origin) ; $i++) { 
                Material::create([
                    'data_id' => $data->id,
                    'origin' => $request->origin[$i],
                    'kind' => $request->kind[$i],
                    'quanity' => $request->quanity[$i]
                ]);
            }
        }

        if ($request->name[0]) {
            for ($i=0; $i < count($request->name); $i++) { 
                $photo = '';
                if ($request->photo[$i]) {
                    $photo = Storage::disk('public')->put('products', $request->photo[$i]);
                }
                Product::create([
                    'data_id' => $data->id,
                    'name' => $request->name[$i],
                    'photo' => $photo
                ]);
            }
        }
        $request->session()->flash('status', 'Data berhasil disimpan');
        return redirect(route('adminBidang.listData', ['user' => $request->user_id]));
    }

}
