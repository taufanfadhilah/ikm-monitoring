<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\CategoryLevel;
use App\District;
use App\User;

class UserController extends Controller
{
    public function edit()
    {
        $categories = Category::all();
        $categoryLevels = CategoryLevel::where([
            ['sub_category', '=', '']
        ])->get();
        $districts = District::all();
        return view('user.edit', [
            'categories' => $categories,
            'categoryLevels' => $categoryLevels,
            'districts' => $districts
        ]);
    }

    public function update(Request $request, User $user)
    {
        $user->update($request->all());
        $request->session()->flash('status', 'Profil berhasil diperbaharui');
        return redirect(route('user.edit'));
    }
}
