<?php

namespace App\Http\Controllers;

use App\Attendance;
use Illuminate\Http\Request;
use App\Facility;
use App\User;
use DB;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('attendance.index');
    }

    public function facility(Facility $facility)
    {
        return view('admin.attendance.index', [
            'facility' => $facility
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Facility $facility)
    {
        $attendances = Attendance::where('facility_id', $facility->id)->pluck('user_id');
        $users = User::where('role', 'IKM')->whereNotIn('id', $attendances)->get();
        
        return view('admin.attendance.create', [
            'users' => $users,
            'facility' => $facility
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->product_id) {
            Attendance::create($request->all());
            $request->session()->flash('status', 'Data berhasil disimpan');
        }
        return redirect(route('attendance.create', ['facility' => $request->facility_id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show(Attendance $attendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendance $attendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $attendance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $attendance)
    {
        $attendance->delete();
        // $request->session()->flash('status', 'Data berhasil dihapus');
        return redirect(route('facility.index'));
    }

    public function product(Facility $facility, User $user)
    {
        return view('admin.attendance.product', [
            'user' => $user,
            'facility' => $facility
        ]);
    }
}
