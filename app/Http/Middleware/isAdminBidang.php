<?php

namespace App\Http\Middleware;

use Closure;

class isAdminBidang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if (Auth::user()->role != 'Admin Bidang') {
        //     return redirect(route('adminBidang.index'));
        // }
        return $next($request);
    }
}
