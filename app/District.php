<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = ['name'];

    public function Attendance()
    {
        return $this->hasManyThrough('App\Attendance', 'App\User', 'company_district');
    }
}
