<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['data_id', 'name', 'photo', 'production_capacity', 'production_value'];
}
