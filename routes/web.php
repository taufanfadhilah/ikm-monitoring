<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('validate', 'HomeController@validation');

Route::middleware(['auth', 'isVerified'])->group(function () {
    Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::post('dashboard', 'HomeController@dashboardYear')->name('dashboard.year');

    // Category Levels
    Route::get('level2/{id}', 'HomeController@getLevel2')->name('api.getLevel2');
    Route::get('level3/{id}', 'HomeController@getLevel3')->name('api.getLevel3');
    Route::get('level4/{id}', 'HomeController@getLevel4')->name('api.getLevel4');
    Route::get('level5/{id}', 'HomeController@getLevel5')->name('api.getLevel5');

    // Edit Profile
    Route::get('edit', 'UserController@edit')->name('user.edit');
    Route::put('update/{user}', 'UserController@update')->name('user.update');

    // Data 
    Route::resource('data', 'DataController');

    // Attendance
    Route::get('attendance', 'AttendanceController@index')->name('attendance.index');
});

Route::prefix('adminBidang')->middleware(['auth'])->group(function(){
    Route::get('/', 'AdminBidangController@index')->name('adminBidang.index');
    Route::get('/grafik/{district}', 'AdminBidangController@detailChart')->name('adminBidang.detailChart');

    // User
    Route::get('approve', 'AdminBidangController@getApprove')->name('adminBidang.getApprove');
    Route::post('approve', 'AdminBidangController@doApprove')->name('adminBidang.approve');
    Route::get('ikm', 'AdminBidangController@getIKM')->name('adminBidang.getIKM');
    Route::get('ikm/{user}', 'AdminBidangController@detailIKM')->name('adminBidang.detailIKM');
    Route::put('ikm/{user}', 'AdminBidangController@updateIKM')->name('adminBidang.updateIKM');
    Route::get('ikmDelete/{user}', 'AdminBidangController@deleteIKM')->name('adminBidang.deleteIKM');
    
    // Data
    Route::get('listData/{user}', 'AdminBidangController@listData')->name('adminBidang.listData');
    Route::get('detailData/{data}', 'AdminBidangController@detailData')->name('adminBidang.detailData');
    Route::get('data/{user}', 'AdminBidangController@addData')->name('adminBidang.addData');
    Route::post('data', 'AdminBidangController@storeData')->name('adminBidang.storeData');

    // Facility
    Route::resource('facility', 'FacilityController');

    // Attendance
    Route::resource('attendance', 'AttendanceController')->except(['create', 'index']);
    Route::get('attendance/create/{facility}', 'AttendanceController@create')->name('attendance.create');
    Route::get('attendance/facility/{facility}', 'AttendanceController@facility')->name('attendance.facility');
    Route::get('attendance/product/{facility}/{user}', 'AttendanceController@product')->name('attendance.product');
});

Route::prefix('superAdmin')->middleware(['auth'])->group(function(){
    Route::get('/', 'SuperAdminController@index')->name('superAdmin.index');
    Route::get('/grafik/{district}', 'SuperAdminController@detailChart')->name('superAdmin.detailChart');
    // Admin Bidang
    Route::get('/adminBidang', 'SuperAdminController@adminBidang')->name('superAdmin.adminBidang.index');
    Route::get('/adminBidang/create', 'SuperAdminController@adminBidangCreate')->name('superAdmin.adminBidang.create');
    Route::post('/adminBidang', 'SuperAdminController@adminBidangStore')->name('superAdmin.adminBidang.store');
    Route::get('/adminBidang/edit/{user}', 'SuperAdminController@adminBidangEdit')->name('superAdmin.adminBidang.edit');
    Route::put('/adminBidang/edit/{user}', 'SuperAdminController@adminBidangUpdate')->name('superAdmin.adminBidang.update');
    Route::delete('adminBidang/{user}', 'SuperAdminController@adminBidangDestroy')->name('superAdmin.adminBidang.destroy');

    // Category
    Route::resource('/category', 'CategoryController');

    // Facility Category
    Route::get('/facilityCategory', 'SuperAdminController@facilityCategory')->name('superAdmin.facilityCategory.index');
    Route::get('/facilityCategory/create', 'SuperAdminController@facilityCategoryCreate')->name('superAdmin.facilityCategory.create');
    Route::post('/facilityCategory', 'SuperAdminController@facilityCategoryStore')->name('superAdmin.facilityCategory.store');
    Route::get('/facilityCategory/edit/{facilityCategory}', 'SuperAdminController@facilityCategoryEdit')->name('superAdmin.facilityCategory.edit');
    Route::put('/facilityCategory/edit/{facilityCategory}', 'SuperAdminController@facilityCategoryUpdate')->name('superAdmin.facilityCategory.update');
    Route::delete('/facilityCategory/{facilityCategory}', 'SuperAdminController@facilityCategoryDestroy')->name('superAdmin.facilityCategory.destroy');
});