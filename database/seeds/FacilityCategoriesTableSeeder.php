<?php

use Illuminate\Database\Seeder;
use App\FacilityCategory;
class FacilityCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $facilityCategories = ['Pelatihan', 'Sertifikasi'];

        foreach ($facilityCategories as $facility) {
            FacilityCategory::create([
                'name' => $facility
            ]);
        }
    }
}
