<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Super Admin',
            'email' => 'superAdmin@ikm.com',
            'password' => bcrypt('secret123'),
            'role' => 'Super Admin',
            'isVerified' => 1,
            'owner' => 'INDAG'
        ]);

        User::create([
            'name' => 'Admin Bidang',
            'email' => 'adminBidang@ikm.com',
            'password' => bcrypt('secret123'),
            'role' => 'Admin Bidang',
            'isVerified' => 1,
            'owner' => 'INDAG'
        ]);
    }
}
