<?php

use Illuminate\Database\Seeder;
use App\District;
class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->delete();
        $districts = [
            'Kabupaten Bandung',
            'Kabupaten Bandung Barat',
            'Kabupaten Bekasi',
            'Kabupaten Bogor',
            'Kabupaten Ciamis',
            'Kabupaten Cianjur',
            'Kabupaten Cirebon',
            'Kabupaten Garut',
            'Kabupaten Indramayu',
            'Kabupaten Karawang',
            'Kabupaten Kuningan',
            'Kabupaten Majalengka',
            'Kabupaten Pangandaran',
            'Kabupaten Pangandaran',
            'Kabupaten Subang',
            'Kabupaten Sukabumi',
            'Kabupaten Sumedang',
            'Kabupaten Tasikmalaya',
            'Kota Bandung',
            'Kota Banjar',
            'Kota Bekasi',
            'Kota Bogor',
            'Kota Cimahi',
            'Kota Cirebon',
            'Kota Depok',
            'Kota Sukabumi',
            'Kota Tasikmalaya'
        ];
        foreach ($districts as $district) {
            District::create([
                'name' => $district
            ]);
        }
    }
}
