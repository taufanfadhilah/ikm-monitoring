<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        $categories = [
            'Tektil',
            'Kuliner'
        ];
        foreach ($categories as $category ) {
            Category::create([
                'name' => $category
            ]);
        }
    }
}
