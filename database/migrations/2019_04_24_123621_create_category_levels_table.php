<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_levels', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('category');
            $table->string('sub_category');
            $table->string('group');
            $table->string('sub_group');
            $table->string('detail_group');
            $table->string('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_levels');
    }
}
