<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('year', 4);
            $table->integer('investment')->default(0)->nullable();
            $table->integer('wni_male')->default(0)->nullable();
            $table->integer('wni_female')->default(0)->nullable();
            $table->integer('wna_male')->default(0)->nullable();
            $table->integer('wna_female')->default(0)->nullable();
            $table->integer('labor_total')->default(0)->nullable();
            $table->string('industrial_permit_number')->nullable();
            $table->date('industrial_permit_period')->nullable();
            $table->string('siup_number')->nullable();
            $table->date('siup_period')->nullable();
            $table->string('tdp_number')->nullable();
            $table->date('tdp_period')->nullable();
            $table->string('nib_number')->nullable();
            $table->date('nib_period')->nullable();
            $table->string('halal_number')->nullable();
            $table->date('halal_period')->nullable();
            $table->string('sni_number')->nullable();
            $table->date('sni_period')->nullable();
            $table->string('iso_number')->nullable();
            $table->date('iso_period')->nullable();
            $table->string('pirt_number')->nullable();
            $table->date('pirt_period')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
}
