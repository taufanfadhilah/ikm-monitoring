<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('role', ['IKM', 'Admin Bidang', 'Super Admin'])->default('IKM');
            $table->boolean('isVerified')->default(false);
            $table->string('owner')->nullable();
            $table->enum('legality', ['PT.', 'CV.', 'PO.', 'PD.', ''])->nullable();
            $table->string('year_establish', 4)->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('category_level_1')->nullable();
            $table->unsignedBigInteger('category_level_2')->nullable();
            $table->unsignedBigInteger('category_level_3')->nullable();
            $table->unsignedBigInteger('category_level_4')->nullable();
            $table->unsignedBigInteger('category_level_5')->nullable();
            $table->unsignedBigInteger('company_district')->nullable();
            $table->string('company_address')->nullable();
            $table->string('company_postal', 6)->nullable();
            $table->unsignedBigInteger('branch_district')->nullable();
            $table->string('branch_address')->nullable();
            $table->string('branch_postal', 6)->nullable();
            $table->string('contact_email')->nullable();
            $table->string('phone', 15)->nullable();
            $table->string('website')->nullable();
            $table->string('social_media')->nullable();
            $table->softDeletes();
            
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('category_level_1')->references('id')->on('category_levels');
            $table->foreign('category_level_2')->references('id')->on('category_levels');
            $table->foreign('category_level_3')->references('id')->on('category_levels');
            $table->foreign('category_level_4')->references('id')->on('category_levels');
            $table->foreign('category_level_5')->references('id')->on('category_levels');
            $table->foreign('company_district')->references('id')->on('districts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
