<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('data_id');
            $table->enum('category', ['Dalam Negeri', 'Luar Negeri', 'E-Commerce'])->default('Dalam Negeri');
            $table->string('location')->nullable();
            $table->integer('value')->default(0);
            $table->timestamps();

            $table->foreign('data_id')->references('id')->on('datas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketings');
    }
}
