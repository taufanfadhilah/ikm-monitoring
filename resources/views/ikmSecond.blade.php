@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <h3>Tambah Data IKM</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form action="">
                            <div class="form-group">
                                <b>Jenis Fasilitas Yang Pernah Diterima</b>
                                <div class="row">
                                    <div class="col-md-3">
                                        <p>Jenis</p>
                                        <select class="form-control">
                                            <option value="">Pelatihan</option>
                                            <option value="">Pameran</option>
                                            <option value="">Setifikasi</option>
                                            <option value="">Kemasan</option>
                                            <option value="">Lain-lain</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <p>Nama</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-3">
                                        <p>Instansi</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-3">
                                        <p>Waktu Pelaksanaan</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-12 text-right m-5">
                                    <a href="">Tambah data</a>
                                </div>
                            </div>
                            <div class="form-group">
                                {{-- <button class="btn btn-primary full-width">Simpan</button> --}}
                                <a href="{{route('dashboard')}}" class="btn btn-primary full-width">
                                    Simpan
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
