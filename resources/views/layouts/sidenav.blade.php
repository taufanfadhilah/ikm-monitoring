<aside class="main-sidebar hidden-print " >
    <section class="sidebar" id="sidebar-scroll">
        
        <div class="user-panel">
            <div class="f-left image"><img src="{{asset('assets/images/avatar-1.png')}}" alt="User Image" class="img-circle"></div>
            <div class="f-left info">
                <p>{{Auth::user()->owner}}</p>
                <p class="designation">{{Auth::user()->name}} <i class="icofont icofont-caret-down m-l-5"></i></p>
            </div>
        </div>
        <!-- sidebar profile Menu-->
        <ul class="nav sidebar-menu extra-profile-list">
            <li>
                <a class="waves-effect waves-dark" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    <i class="icon-logout"></i>
                    <span class="menu-text">Logout</span>
                    <span class="selected"></span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
        <!-- Sidebar Menu-->
        <ul class="sidebar-menu">
            <li class="nav-level">Navigation</li>
            <li class="active treeview">
                <a class="waves-effect waves-dark" href="{{route('dashboard')}}">
                    <i class="icon-speedometer"></i><span> Dashboard</span>
                </a>                
            </li>
            <li class="nav-level">Data</li>
            <li class="treeview">
                <a class="waves-effect waves-dark" href="{{route('user.edit')}}">
                    <i class="icon-user"></i><span> Ubah Profil</span>
                </a>                
            </li>
            <li class="treeview">
                <a class="waves-effect waves-dark" href="{{route('data.create')}}">
                    <i class="icon-note"></i><span> Tambah Data IKM Tahunan</span>
                </a>                
            </li>
            <li class="treeview">
                <a class="waves-effect waves-dark" href="{{route('attendance.index')}}">
                    <i class="icon-list"></i><span> Riwayat Fasilitas</span>
                </a>                
            </li>
        </ul>
    </section>
</aside>