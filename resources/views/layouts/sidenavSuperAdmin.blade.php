<aside class="main-sidebar hidden-print " >
    <section class="sidebar" id="sidebar-scroll">
        
        <div class="user-panel">
            <div class="f-left image"><img src="{{asset('assets/images/avatar-1.png')}}" alt="User Image" class="img-circle"></div>
            <div class="f-left info">
                <p>{{Auth::user()->owner}}</p>
                <p class="designation">{{Auth::user()->role}} <i class="icofont icofont-caret-down m-l-5"></i></p>
            </div>
        </div>
        <!-- sidebar profile Menu-->
        <ul class="nav sidebar-menu extra-profile-list">
            <li>
                <a class="waves-effect waves-dark" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    <i class="icon-logout"></i>
                    <span class="menu-text">Logout</span>
                    <span class="selected"></span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
        <!-- Sidebar Menu-->
        <ul class="sidebar-menu">
            <li class="nav-level">Navigation</li>
            <li class="active treeview">
                <a class="waves-effect waves-dark" href="{{route('superAdmin.index')}}">
                    <i class="icon-speedometer"></i><span> Dashboard</span>
                </a>                
            </li>
            <li class="nav-level">Data</li>
            <li class="treeview">
                <a class="waves-effect waves-dark" href="{{route('superAdmin.adminBidang.index')}}">
                    <i class="icon-user-following"></i><span> Admin Bidang</span>
                </a>                
            </li>
            <li class="treeview">
                <a class="waves-effect waves-dark" href="{{route('category.index')}}">
                    <i class="icon-list"></i><span> Bidang / UPTD Pembina</span>
                </a>                
            </li>
            <li class="treeview">
                <a class="waves-effect waves-dark" href="{{route('superAdmin.facilityCategory.index')}}">
                    <i class="icon-list"></i><span> Jenis Fasilitas</span>
                </a>                
            </li>
        </ul>
    </section>
</aside>