@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                @include('layouts.alert')
                <h3>Ubah Profil</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('user.update', ['user' => Auth::user()->id])}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">Nama Pemilik</label>
                                <input class="form-control" style="text-transform: uppercase" type="text" name="owner" value="{{Auth::user()->owner}}" required/>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="">Badan Hukum</label>
                                        <select name="legality" id="" class="form-control" required>
                                            @if (Auth::user()->legality == 'PT.')
                                                <option value="PT." selected>PT.</option>    
                                            @else
                                                <option value="PT.">PT.</option>
                                            @endif
                                            @if (Auth::user()->legality == 'CV.')
                                                <option value="CV." selected>CV.</option>
                                            @else
                                                <option value="CV.">CV.</option>
                                            @endif
                                            @if (Auth::user()->legality == 'PO.')
                                                <option value="PO." selected>PO.</option>    
                                            @else
                                                <option value="PO.">PO.</option>
                                            @endif
                                            @if (Auth::user()->legality == 'PD.')
                                                <option value="PD." selected>PD.</option>
                                            @else
                                                <option value="PD.">PD.</option>
                                            @endif
                                            @if (Auth::user()->legality == '' || Auth::user()->legality == 'Tidak Berbadan Hukum')
                                                <option value="" selected>Tidak Berbadan Hukum</option>    
                                            @else
                                                <option value="">Tidak Berbadan Hukum</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="">Nama Perusahaan</label>
                                        <input class="form-control" style="text-transform: uppercase" type="text" name="name" value="{{Auth::user()->name}}" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Tahun Pendirian</label>
                                <input class="form-control" type="text" name="year_establish" value="{{Auth::user()->year_establish}}" id="datepicker" required/>
                            </div>
                            <div class="form-group">
                                <label for="">Bidang / UPTD Pembina</label>
                                <select name="category_id" id="" class="form-control" required>
                                    @foreach ($categories as $category)
                                        @if (Auth::user()->category_id == $category->id)
                                            <option value="{{$category->id}}" selected>{{$category->name}}</option>
                                        @else
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Jenis Usaha</b>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Level 1 (Terpilih: <b>
                                            @if (Auth::user()->CategoryLevel1)
                                                {{Auth::user()->CategoryLevel1->title}}
                                            @else
                                                -
                                            @endif    
                                        </b>)</p>
                                        <select class="form-control" name="category_level_1" id="level1">
                                            <option value="" disabled selected>Pilih Level 1</option>
                                            @foreach ($categoryLevels as $categoryLevel)
                                                <option value="{{$categoryLevel->id}}" category="{{$categoryLevel->category}}" id="level1-{{$categoryLevel->id}}">{{$categoryLevel->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Level 2 (Terpilih: <b>
                                            @if (Auth::user()->CategoryLevel2)
                                                {{Auth::user()->CategoryLevel2->title}}
                                            @else
                                                -
                                            @endif    
                                        </b>)</p>
                                        <select class="form-control" name="category_level_2" id="level2">
                                            <option value="" disabled selected>Pilih Level 2</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Level 3 (Terpilih: <b>
                                            @if (Auth::user()->CategoryLevel3)
                                                {{Auth::user()->CategoryLevel3->title}}
                                            @else
                                                -
                                            @endif    
                                        </b>)</p>
                                        <select class="form-control" name="category_level_3" id="level3">
                                            <option value="" disabled selected>Pilih Level 3</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Level 4 (Terpilih: <b>
                                            @if (Auth::user()->CategoryLevel4)
                                                {{Auth::user()->CategoryLevel4->title}}
                                            @else
                                                -
                                            @endif    
                                        </b>)</p>
                                        <select class="form-control" name="category_level_4" id="level4">
                                            <option value="" disabled selected>Pilih Level 4</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Level 5 (Terpilih: <b>
                                            @if (Auth::user()->CategoryLevel5)
                                                {{Auth::user()->CategoryLevel5->title}}
                                            @else
                                                -
                                            @endif    
                                        </b>)</p>
                                        <select class="form-control" name="category_level_5" id="level5">
                                            <option value="" disabled selected>Pilih Level 5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Alamat Perusahaan</b>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Kota / Kab</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Kode Pos</p>
                                    </div>
                                    <div class="col-md-6">
                                        <select class="form-control" name="company_district" required>
                                            @foreach ($districts as $district)
                                                @if (Auth::user()->company_district == $district->id)
                                                    <option value="{{$district->id}}" selected>{{$district->name}}</option>
                                                @else
                                                    <option value="{{$district->id}}">{{$district->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="number" name="company_postal" value="{{Auth::user()->company_postal}}" required />
                                    </div>
                                </div>
                                <label for="">Alamat</label>
                                <textarea class="form-control" name="company_address" required>{{Auth::user()->company_address}}</textarea>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Alamat Pabrik</b>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Kota / Kab</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Kode Pos</p>
                                    </div>
                                    <div class="col-md-6">
                                        <select class="form-control" name="branch_district">
                                            @foreach ($districts as $district)
                                                @if (Auth::user()->branch_district == $district->id)
                                                    <option value="{{$district->id}}" selected>{{$district->name}}</option>
                                                @else
                                                    <option value="{{$district->id}}">{{$district->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="branch_postal"  value="{{Auth::user()->branch_postal}}" />
                                    </div>
                                </div>
                                <label for="">Alamat</label>
                                <textarea class="form-control" name="branch_address"></textarea>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Kontak</b>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Email</p>
                                        <input class="form-control" type="email" name="contact_email" value="{{Auth::user()->contact_email}}" required />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Website</p>
                                        <input class="form-control" type="url" name="website"  value="{{Auth::user()->website}}" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Telpon</p>
                                        <input class="form-control" type="tel" name="phone"  value="{{Auth::user()->phone}}" required/>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Sosial Media</p>
                                        <input class="form-control" type="text" name="social_media"  value="{{Auth::user()->social_media}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary full-width">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
    <script>
        $("#datepicker").datepicker({
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years"
        });

        $("#level1").change(function(){
            var id = $("#level1").val()
            var select = document.getElementById("level2")
            var category = document.getElementById("level1-"+id).getAttribute('category')
            var route = "{{route('api.getLevel2', ['id' => ':category'])}}"
            route = route.replace(':category', category)
            $.ajax({url: route, success: function(result){
                $("#level2").empty();
                var defaultOption = document.createElement( 'option' );
                defaultOption.value = null;
                defaultOption.text = 'Pilih Level 2';
                defaultOption.disabled = true;
                defaultOption.selected = true;
                select.add( defaultOption );

                result.forEach(value => {
                    option = document.createElement( 'option' );
                    option.id = 'level2-'+value.id
                    option.value = value.id;
                    option.text = value.title;
                    option.setAttribute('sub_category', value.sub_category);
                    select.add( option );
                });
            }});
        });

        $("#level2").change(function(){
            var id = $("#level2").val()
            var select = document.getElementById("level3")
            var category = document.getElementById("level2-"+id).getAttribute('sub_category')
            var route = "{{route('api.getLevel3', ['id' => ':category'])}}"
            route = route.replace(':category', category)
            $.ajax({url: route, success: function(result){
                $("#level3").empty();
                var defaultOption = document.createElement( 'option' );
                defaultOption.value = null;
                defaultOption.text = 'Pilih Level 3';
                defaultOption.disabled = true;
                defaultOption.selected = true;
                select.add( defaultOption );
                result.forEach(value => {
                    var option = document.createElement( 'option' );
                    option.id = 'level3-'+value.id
                    option.value = value.id;
                    option.text = value.title;
                    option.setAttribute('group', value.group);
                    select.add( option );
                });
            }});
        });

        $("#level3").change(function(){
            var id = $("#level3").val()
            var select = document.getElementById("level4")
            var category = document.getElementById("level3-"+id).getAttribute('group')
            var route = "{{route('api.getLevel4', ['id' => ':category'])}}"
            route = route.replace(':category', category)
            $.ajax({url: route, success: function(result){
                $("#level4").empty();
                var defaultOption = document.createElement( 'option' );
                defaultOption.value = null;
                defaultOption.text = 'Pilih Level 4';
                defaultOption.disabled = true;
                defaultOption.selected = true;
                select.add( defaultOption );
                
                result.forEach(value => {
                    var option = document.createElement( 'option' );
                    option.id = 'level4-'+value.id
                    option.value = value.id;
                    option.text = value.title;
                    option.setAttribute('sub_group', value.sub_group);
                    select.add( option );
                });
            }});
        });

        $("#level4").change(function(){
            var id = $("#level4").val()
            var select = document.getElementById("level5")
            var category = document.getElementById("level4-"+id).getAttribute('sub_group')
            var route = "{{route('api.getLevel5', ['id' => ':category'])}}"
            route = route.replace(':category', category)
            $.ajax({url: route, success: function(result){
                $("#level5").empty();
                var defaultOption = document.createElement( 'option' );
                defaultOption.value = null;
                defaultOption.text = 'Pilih Level 5';
                defaultOption.disabled = true;
                defaultOption.selected = true;
                select.add( defaultOption );

                result.forEach(value => {
                    var option = document.createElement( 'option' );
                    option.id = 'level5-'+value.id
                    option.value = value.id;
                    option.text = value.title;
                    option.setAttribute('sub_group', value.sub_group);
                    select.add( option );
                });
            }});
        });
    </script>
@endpush