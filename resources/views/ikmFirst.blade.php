@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <h3>Tambah Data IKM</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form action="">
                            <div class="form-group">
                                <label for="">Tahun</label>
                                <input class="form-control" type="number">
                            </div>
                            <div class="form-group">
                                <label for="">Kapasitas Produksi</label>
                                <input class="form-control" type="text" />
                            </div>
                            <div class="form-group">
                                <label for="">Investasi</label>
                                <input class="form-control" type="text" />
                            </div>
                            <div class="form-group">
                                <label for="">Nilai Produksi Tahunan</label>
                                <input class="form-control" type="number" />
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Jenis Produk</b>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Nama</p>
                                        <input class="form-control" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <p>Foto</p>
                                        <input class="form-control" type="file">
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <u class="text-primary">Tambah Jenis Produk</u>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Sumber Bahan Baku</b>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Asal</p>
                                        <select class="form-control" name="">
                                            <option value="">Lokal</option>
                                            <option value="">Import</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Lokal : Jenis</p>
                                        <input class="form-control" type="text">
                                    </div>
                                    <div class="col-md-4">
                                        <p>Lokal : Jumlah</p>
                                        <input class="form-control" type="number">
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <u class="text-primary">Tambah Bahan Baku</u>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Jumlah Tenaga Kerja</b>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>WNI Pria</p>
                                        <input class="form-control" type="number" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>WNI Wanita</p>
                                        <input class="form-control" type="number" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>WNA Pria</p>
                                        <input class="form-control" type="number" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>WNA Wanita</p>
                                        <input class="form-control" type="number" />
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Jenis Perizinan</b>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Izin Usaha Industri</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">SIUP</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">TDP</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <b>Jenis Standarisasi</b>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Halal</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">SNI</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">ISO</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">PIRT</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Segmen Pemasaran</b>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Dalam Negeri</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Lokasi</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nilai</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <u class="text-primary">Tambah Pemasaran Dalam Negeri</u>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Luar Negeri</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Lokasi</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nilai</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <u class="text-primary">Tambah Pemasaran Luar Negeri</u>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">E-Commerce</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nama</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nilai</p>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <u class="text-primary">Tambah E-Commerce</u>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="{{route('dashboard')}}" class="btn btn-primary full-width">Simpan
                                    {{-- <button class="btn btn-primary full-width">Selanjutnya</button> --}}
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
