@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-8">
                        <h3>{{$user->legality .' '. $user->name}}</h3>
                    </div>
                    <div class="col-md-4">
                        <a href="{{route('adminBidang.listData', ['user' => $user->id])}}" class="btn btn-success full-width">Lihat Data Tahunan</a>
                    </div>
                </div>
                @if (!isset($user->CategoryLevel5->title))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert">
                            Data Belum Lengkap
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('adminBidang.updateIKM', ['user' => $user->id])}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">Nama Pemilik</label>
                                <input class="form-control" style="text-transform: uppercase" type="text" name="owner" value="{{$user->owner}}" required/>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="">Badan Hukum</label>
                                        <select name="legality" id="" class="form-control" required>
                                            @if ($user->legality == 'PT.')
                                                <option value="PT." selected>PT.</option>    
                                            @else
                                                <option value="PT.">PT.</option>
                                            @endif
                                            @if ($user->legality == 'CV.')
                                                <option value="CV." selected>CV.</option>
                                            @else
                                                <option value="CV.">CV.</option>
                                            @endif
                                            @if ($user->legality == 'PO.')
                                                <option value="PO." selected>PO.</option>    
                                            @else
                                                <option value="PO.">PO.</option>
                                            @endif
                                            @if ($user->legality == 'PD.')
                                                <option value="PD." selected>PD.</option>
                                            @else
                                                <option value="PD.">PD.</option>
                                            @endif
                                            @if ($user->legality == '' || $user->legality == 'Tidak Berbadan Hukum')
                                                <option value="Tidak Berbadan Hukum" selected>Tidak Berbadan Hukum</option>    
                                            @else
                                                <option value="Tidak Berbadan Hukum">Tidak Berbadan Hukum</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="">Nama Perusahaan</label>
                                        <input class="form-control" style="text-transform: uppercase" type="text" name="name" value="{{$user->name}}" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Tahun Pendirian</label>
                                <input class="form-control" type="text" name="year_establish" value="{{$user->year_establish}}" id="datepicker" required/>
                            </div>
                            <div class="form-group">
                                <label for="">Bidang Usaha</label>
                                <select name="category_id" id="" class="form-control" required>
                                    @foreach ($categories as $category)
                                        @if ($user->category_id == $category->id)
                                            <option value="{{$category->id}}" selected>{{$category->name}}</option>
                                        @else
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Jenis Usaha</b>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Level 1 (Terpilih: <b>{{isset($user->CategoryLevel1->title) ? $user->CategoryLevel1->title : '-'}}</b>)</p>
                                        <select class="form-control" name="category_level_1" id="level1">
                                            <option value="" disabled selected>Pilih Level 1</option>
                                            @foreach ($categoryLevels as $categoryLevel)
                                                <option value="{{$categoryLevel->id}}" category="{{$categoryLevel->category}}" id="level1-{{$categoryLevel->id}}">{{$categoryLevel->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Level 2 (Terpilih: <b>{{isset($user->CategoryLevel2->title) ? $user->CategoryLevel2->title : '-'}}</b>)</p>
                                        <select class="form-control" name="category_level_2" id="level2">
                                            <option value="" disabled selected>Pilih Level 2</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Level 3 (Terpilih: <b>{{isset($user->CategoryLevel3->title) ? $user->CategoryLevel3->title : '-'}}</b>)</p>
                                        <select class="form-control" name="category_level_3" id="level3">
                                            <option value="" disabled selected>Pilih Level 3</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Level 4 (Terpilih: <b>{{isset($user->CategoryLevel4->title) ? $user->CategoryLevel4->title : '-'}}</b>)</p>
                                        <select class="form-control" name="category_level_4" id="level4">
                                            <option value="" disabled selected>Pilih Level 4</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Level 5 (Terpilih: <b>{{isset($user->CategoryLevel5->title) ? $user->CategoryLevel5->title : '-'}}</b>)</p>
                                        <select class="form-control" name="category_level_5" id="level5">
                                            <option value="" disabled selected>Pilih Level 5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Alamat Perusahaan</b>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Kota / Kab</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Kode Pos</p>
                                    </div>
                                    <div class="col-md-6">
                                        <select class="form-control" name="company_district" required>
                                            @foreach ($districts as $district)
                                                @if ($user->company_district == $district->id)
                                                    <option value="{{$district->id}}" selected>{{$district->name}}</option>
                                                @else
                                                    <option value="{{$district->id}}">{{$district->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="number" name="company_postal" value="{{$user->company_postal}}" required />
                                    </div>
                                </div>
                                <label for="">Alamat</label>
                                <textarea class="form-control" name="company_address" required>{{$user->company_address}}</textarea>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Alamat Pabrik</b>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Kota / Kab</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Kode Pos</p>
                                    </div>
                                    <div class="col-md-6">
                                        <select class="form-control" name="branch_district">
                                            @foreach ($districts as $district)
                                                @if ($user->branch_district == $district->id)
                                                    <option value="{{$district->id}}" selected>{{$district->name}}</option>
                                                @else
                                                    <option value="{{$district->id}}">{{$district->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="branch_postal"  value="{{$user->branch_postal}}" />
                                    </div>
                                </div>
                                <label for="">Alamat</label>
                                <textarea class="form-control" name="branch_address">Jl. Dago</textarea>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Kontak</b>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Email</p>
                                        <input class="form-control" type="email" name="contact_email" value="{{$user->contact_email}}" required />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Website</p>
                                        <input class="form-control" type="url" name="website"  value="{{$user->website}}" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Telpon</p>
                                        <input class="form-control" type="tel" name="phone"  value="{{$user->phone}}" required/>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Sosial Media</p>
                                        <input class="form-control" type="text" name="social_media"  value="{{$user->social_media}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary full-width">Simpan Data</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
