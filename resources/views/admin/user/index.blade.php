@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <h3>Daftar IKM</h3>
                <div class="row">
                @include('layouts.alert')
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Pemilik</th>
                                    <th>Nama Perusahaan</th>
                                    <th>Waktu Daftar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $index => $user)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$user->owner}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->created_at->format('d M Y')}}</td>
                                        <td>
                                            <a href="{{route('adminBidang.detailIKM', ['user' => $user->id])}}">
                                                <button class="btn btn-primary">Detail</button>
                                            </a>
                                            <a href="{{route('adminBidang.deleteIKM', ['user' => $user->id])}}">
                                                <button class="btn btn-danger">Hapus</button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
