@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-9">
                        <h3>Daftar Hadir {{$facility->name}}</h3>
                    </div>
                    <div class="col-md-3">
                        <a href="{{route('attendance.create', ['facility' => $facility->id])}}" class="btn btn-success full-width">Tambah Kehadiran</a>
                    </div>
                </div>
                @include('layouts.alert')
                <div class="row">
                    <div class="col-md-12">
                        <table id="myTable" class="display">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Perusahaan</th>
                                    <th>Nama Pemilik</th>
                                    <th>Alamat</th>
                                    <th>Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($facility->Attendance as $index => $attendance)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$attendance->User->legality}} {{$attendance->User->name}}</td>
                                        <td>{{$attendance->User->owner}}</td>
                                        <td>{{$attendance->User->company_address}}, {{$attendance->User->District->name}}</td>
                                        <td>
                                            <form action="{{route('attendance.destroy', ['attendance' => $attendance->id])}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ]
            } );
        } );
    </script>
@endpush