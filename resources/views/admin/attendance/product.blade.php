@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-9">
                        <h3>Daftar Produk {{$user->legality}} {{$user->name}}</h3>
                    </div>
                </div>
                @include('layouts.alert')
                <form action="{{route('attendance.store')}}" method="POST">
                    @csrf
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <input type="hidden" name="facility_id" value="{{$facility->id}}">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Tahun</th>
                                        <th>Foto</th>
                                        <th>Nama Produk</th>
                                        <th>Tambah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($user->Data as $data)
                                        @foreach ($data->Product as $product)
                                            <tr>
                                                <td>{{$data->year}}</td>
                                                <td><img src="{{asset('storage/'.$product->photo)}}" alt="{{$product->name}}" class="img-thumbnail" style="max-width: 100px"></td>
                                                <td>{{$product->name}}</td>
                                                <td>
                                                    <input type="radio" name="product_id" value="{{$product->id}}" class="form-control">
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-success full-width" onclick="history.go(-1);">Tambah dan Kembali Ke Halaman Kehadiran</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
