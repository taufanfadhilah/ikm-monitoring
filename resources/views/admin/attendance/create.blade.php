@extends('layouts.app')

@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endpush

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                @include('layouts.alert')
                <h3>Tambah Kehadiran</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('attendance.store')}}" method="POST">
                            @csrf
                            <input type="hidden" name="facility_id" value="{{$facility->id}}">
                            <table id="myTable">
                                <thead>
                                    <tr>
                                        <th>Nama Perusahaan</th>
                                        <th>Nama Pemilik</th>
                                        <th>Alamat</th>
                                        {{-- <th>Tambah</th> --}}
                                        <th>Lihat Produksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>
                                                @if ($user->legality != 'Tidak Berbadan Hukum')
                                                    {{$user->legality}}
                                                @endif
                                                {{$user->name}}</td>
                                            <td>{{$user->owner}}</td>
                                            <td>
                                                @isset ($user->District->name)
                                                    {{$user->company_address . ', ' . $user->District->name}}
                                                @else
                                                    -
                                                @endisset
                                            </td>
                                            {{-- <td>
                                                <div class="rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
                                                    <label class="input-checkbox checkbox-primary">
                                                        <input type="checkbox" id="checkbox" value="{{$user->id}}" name="user_id[]">
                                                        <span class="checkbox"></span>
                                                    </label>
                                                </div>
                                            </td> --}}
                                            <td>
                                                <a href="{{route('attendance.product', ['user' => $user->id, 'facility' => $facility->id])}}" class="btn btn-primary">
                                                    Lihat
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="form-group">
                                {{-- <button type="submit" class="btn btn-primary full-width">Simpan</button> --}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>
@endpush