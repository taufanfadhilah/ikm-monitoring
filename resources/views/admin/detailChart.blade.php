@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <h3>Grafik di {{$district->name}}</h3>
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="myChart" width="400" height="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <h3>Data Peserta di {{$district->name}}</h3>
                <div class="row">
                    <div class="col-md-12">
                        {{-- @foreach ($facilities as $facility)
                            {{$facility}}
                            <br>
                        @endforeach --}}
                        <table id="example" class="display">
                            <thead>
                                <tr>
                                    <th>No. </th>
                                    <th>Nama IKM</th>
                                    <th>Nama Pemilik</th>
                                    <th>Alamat</th>
                                    <th>Jenis Fasilitas</th>
                                    <th>Detail Fasilitas</th>
                                    <th>Jenis Produk</th>
                                    <th>Waktu</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($district->Attendance as $index => $attendance)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$attendance->User->name}}</td>
                                        <td>{{$attendance->User->owner}}</td>
                                        <td>{{$attendance->User->company_address}}</td>
                                        <td>{{$attendance->Facility->FacilityCategory->name}}</td>
                                        <td>{{$attendance->Facility->name}}</td>
                                        <td>{{$attendance->Product->name}}</td>
                                        <td>{{$attendance->Facility->start_date}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ]
            } );
        } );

        function random_rgba() {
            var o = Math.round, r = Math.random, s = 255;
            return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ', 0.2)';
        }
        var ctx = document.getElementById('myChart');
        var facilities = {!! json_encode($facilities) !!};
        const keys = Object.keys(facilities)
        
        var facilityNames = new Array()
        var facilityAttendances = new Array()
        var facilityColors = new Array()
        for (let index = 0; index < keys.length; index++) {
            const element = keys[index];
            facilityNames.push(facilities[element][0].facility.name)
            facilityAttendances.push(facilities[element].length)
            facilityColors.push(random_rgba())
        }
        
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: facilityNames,
                datasets: [{
                    label: 'peserta',
                    data: facilityAttendances,
                    backgroundColor: facilityColors,
                    borderColor: facilityColors,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endpush