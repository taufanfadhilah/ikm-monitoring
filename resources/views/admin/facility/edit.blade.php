@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                @include('layouts.alert')
                <h3>Tambah Fasilitas</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('facility.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="">Jenis</label>
                                <select name="facility_category_id" class="form-control">
                                    @foreach ($categories as $category)
                                        @if ($category->id == $facility->facility_category_id)
                                            <option value="{{$category->id}}" selected>{{$category->name}}</option>
                                        @else
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input class="form-control" type="text" name="name" value="{{$facility->name}}" required/>
                            </div>
                            <div class="form-group">
                                <label for="">Instansi</label>
                                <select name="category_id" class="form-control">
                                    @foreach ($instances as $instance)
                                        @if ($instance->id == $facility->category_id)
                                            <option value="{{$instance->id}}" selected>{{$instance->name}}</option>
                                        @else
                                            <option value="{{$instance->id}}">{{$instance->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Waktu Mulai</label>
                                        <input class="form-control" type="date" name="start_date" value="{{$facility->start_date}}" required/>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Waktu Berakhir</label>
                                        <input class="form-control" type="date" name="end_date" value="{{$facility->end_date}}" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary full-width">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection