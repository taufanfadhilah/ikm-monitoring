@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                @include('layouts.alert')
                <h3>Tambah Fasilitas</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('facility.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="">Jenis</label>
                                <select name="facility_category_id" class="form-control" required>
                                    @foreach ($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input class="form-control" type="text" name="name" required/>
                            </div>
                            <div class="form-group">
                                <label for="">Instansi</label>
                                {{-- <input class="form-control" type="text" name="instance" required/> --}}
                                <select name="category_id" class="form-control" required>
                                    @foreach ($instances as $instance)
                                        <option value="{{$instance->id}}">{{$instance->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Waktu Mulai</label>
                                        <input class="form-control" type="date" name="start_date" required/>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Waktu Berakhir</label>
                                        <input class="form-control" type="date" name="end_date" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary full-width">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection