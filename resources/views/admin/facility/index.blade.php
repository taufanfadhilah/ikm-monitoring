@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-9">
                        <h3>Daftar Fasilitas</h3>
                    </div>
                    <div class="col-md-3">
                        <a href="{{route('facility.create')}}" class="btn btn-success full-width">Tambah Detail Fasilitas</a>
                    </div>
                </div>
                @include('layouts.alert')
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Jenis Fasilitas</th>
                                    <th>Detail Fasilitas</th>
                                    <th>Waktu</th>
                                    <th>Peserta</th>
                                    <th colspan="3">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($facilities as $index => $facility)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>
                                            @isset ($facility->FacilityCategory->name)
                                                {{$facility->FacilityCategory->name}}
                                                @else
                                                -
                                            @endisset
                                        </td>
                                        <td>{{$facility->name}}</td>
                                        <td>{{$facility->start_date}} - {{$facility->end_date}}</td>
                                        <td>{{count($facility->Attendance)}}</td>
                                        <td>
                                            <a href="{{route('attendance.facility', ['facility' => $facility->id])}}" class="btn btn-success">Kehadiran</a>
                                        </td>
                                        <td>
                                            <a href="{{route('facility.edit', ['facility' => $facility->id])}}" class="btn btn-warning">Ubah</a>
                                        </td>
                                        <td>
                                            <form action="{{route('facility.destroy', ['facility' => $facility->id])}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
