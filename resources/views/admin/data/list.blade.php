@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-9">
                        <h3>Data Tahunan {{$user->legality . ' ' . $user->name}}</h3>
                    </div>
                    <div class="col-md-3">
                        <a href="{{route('adminBidang.addData', ['user' => $user->id])}}" class="btn btn-success full-width">Tambah Data Tahunan</a>
                    </div>
                </div>
                @include('layouts.alert')
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tahun</th>
                                    <th>Investasi</th>
                                    <th colspan="3">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $index => $data)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$data->year}}</td>
                                        <td>{{number_format($data->investment)}}</td>
                                        <td>
                                            <a href="{{route('adminBidang.detailData', ['data' => $data->id])}}" class="btn btn-success">Detail</a>
                                        </td>
                                        <td>
                                            <button class="btn btn-warning">Ubah</button>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger">Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
