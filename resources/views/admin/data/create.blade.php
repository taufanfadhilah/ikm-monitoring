@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <h3>Tambah Data IKM</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('adminBidang.storeData')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <div class="form-group">
                                <label for="">Tahun Pengisian</label>
                                <input class="form-control" type="number" name="year" id="datepicker" required>
                            </div>
                            <div class="form-group">
                                <label for="">Investasi</label>
                                <br>
                                <small><b>Diisi angka tanpa titik dan koma</b></small>
                                <input class="form-control" type="number" name="investment" required/>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Jenis Produk</b>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Nama</p>
                                        <input class="form-control" type="text" name="name[]" required>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Foto</p>
                                        <input class="form-control" type="file" name="photo[]">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>Kapasitas Produksi</p>
                                            <input class="form-control" type="text" style="text-transform: uppercase" name="production_capacity[]" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>Nilai Produksi Tahunan (dalam rupiah)<p>
                                            <input class="form-control" type="number" name="production_value[]" required />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <hr>
                                    </div>
                                    <div id="addProduct"></div>
                                    <div class="col-md-12 text-right">
                                        <u class="text-primary" onclick="addProduct()">Tambah Jenis Produk</u>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Sumber Bahan Baku</b>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Asal</p>
                                        <select class="form-control" name="origin[]" required>
                                            <option value="" disabled selected>-</option>
                                            <option value="Lokal">Lokal</option>
                                            <option value="Import">Import</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Nama Bahan Baku</p>
                                        <input class="form-control" type="text" name="kind[]" required>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Volume</p>
                                        <input class="form-control" type="text" style="text-transform: uppercase" name="quantity[]" required>
                                    </div>
                                </div>
                                <div id="addMaterial"></div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <u class="text-primary" onclick="addMaterial()">Tambah Bahan Baku</u>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Jumlah Tenaga Kerja</b>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>WNI Pria</p>
                                        <input class="form-control" type="number" value="0" name="wni_male" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>WNI Wanita</p>
                                        <input class="form-control" type="number" value="0" name="wni_female" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>WNA Pria</p>
                                        <input class="form-control" type="number" value="0" name="wna_male" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>WNA Wanita</p>
                                        <input class="form-control" type="number" value="0" name="wna_female" />
                                    </div>
                                    <div class="col-md-12">
                                        <p>Total Tenaga Kerja</p>
                                        <input class="form-control" type="number" value="0" name="labor_total" />
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Jenis Perizinan</b>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Izin Usaha Industri</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" name="industrial_permit_number" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="date" name="industrial_permit_period" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">SIUP</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" name="siup_number" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="date" name="siup_period" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">TDP</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" name="tdp_number" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="date" name="tdp_period" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">NIB</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" name="nib_number" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Tanggal Terbit</p>
                                        <input class="form-control" type="date" name="nib_period" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <b>Jenis Sertifikasi</b>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Halal</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" name="halal_number" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="date" name="halal_period" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">SNI</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" name="sni_number" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="date" name="sni_period" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">ISO</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" name="iso_number" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="date" name="iso_period" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">PIRT</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nomor</p>
                                        <input class="form-control" type="text" name="pirt_number" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Masa Berlaku</p>
                                        <input class="form-control" type="date" name="pirt_period" />
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <b>Segmen Pemasaran</b>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Dalam Negeri</label>
                                    </div>
                                    <input type="hidden" name="category[]" value="Dalam Negeri">
                                    <div class="col-md-6">
                                        <p>Lokasi</p>
                                        <input class="form-control" type="text" name="location[]" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nilai (dalam rupiah)</p>
                                        <input class="form-control" type="number" value="0" name="value[]" />
                                    </div>
                                    <div id="addDomestic"></div>
                                    <div class="col-md-12 text-right">
                                        <u class="text-primary" onclick="addDomestic()">Tambah Pemasaran Dalam Negeri</u>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Luar Negeri</label>
                                    </div>
                                    <input type="hidden" name="category[]" value="Luar Negeri">
                                    <div class="col-md-6">
                                        <p>Lokasi</p>
                                        <input class="form-control" type="text" name="location[]" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nilai (dalam rupiah)</p>
                                        <input class="form-control" type="number" value="0" name="value[]" />
                                    </div>
                                    <div id="addOverseas"></div>
                                    <div class="col-md-12 text-right">
                                        <u class="text-primary" onclick="addOverseas()">Tambah Pemasaran Luar Negeri</u>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">E-Commerce</label>
                                    </div>
                                    <input type="hidden" name="category[]" value="E-Commerce">
                                    <div class="col-md-6">
                                        <p>Nama</p>
                                        <input class="form-control" type="text" name="location[]" />
                                    </div>
                                    <div class="col-md-6">
                                        <p>Nilai (dalam rupiah)</p>
                                        <input class="form-control" type="number" value="0" name="value[]" />
                                    </div>
                                    <div id="addECommerce"></div>
                                    <div class="col-md-12 text-right">
                                        <u class="text-primary" onclick="addECommerce()">Tambah E-Commerce</u>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary full-width">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
    <script>
        $("#datepicker").datepicker({
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years"
        });

        function addProduct(){
            document.getElementById('addProduct').innerHTML += 
                                        '<div class="col-md-6">' +
                                            '<p>Nama</p>' +
                                            '<input class="form-control" type="text" name="name[]">' +
                                        '</div>' +
                                        '<div class="col-md-6">' +
                                            '<p>Foto</p>' +
                                            '<input class="form-control" type="file" name="photo[]">' +
                                        '</div>'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<p>Kapasitas Produksi</p>'+
                                                '<input class="form-control" type="text" style="text-transform: uppercase" name="production_capacity[]" required />'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<p>Nilai Produksi Tahunan (dalam rupiah)<p>'+
                                                '<input class="form-control" type="number" name="production_value[]" required />'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<hr>'+
                                        '</div>';
        }

        function addMaterial(){
            document.getElementById('addMaterial').innerHTML += 
                                    '<div class="row">' +
                                        '<div class="col-md-4">' +
                                            '<p>Asal</p>' +
                                            '<select class="form-control" name="origin[]">' +
                                                '<option value="Lokal">Lokal</option>' +
                                                '<option value="Import">Import</option>' +
                                            '</select>' +
                                        '</div>' +
                                        '<div class="col-md-4">' +
                                            '<p>Jenis</p>' +
                                            '<input class="form-control" type="text" name="kind[]">' +
                                        '</div>' +
                                        '<div class="col-md-4">' +
                                            '<p>Volume</p>' +
                                            '<input class="form-control" type="text" style="text-transform: uppercase" name="quantity[]">' +
                                        '</div>'+
                                    '</div>';
        }

        function addDomestic(){
            document.getElementById('addDomestic').innerHTML += 
                                            '<input type="hidden" name="category[]" value="Dalam Negeri">' +
                                            '<div class="col-md-6">' +
                                                '<p>Lokasi</p>' +
                                                '<input class="form-control" type="text" name="location[]" />' +
                                            '</div>' +
                                            '<div class="col-md-6">' +
                                                '<p>Nilai</p>' +
                                                '<input class="form-control" type="number" value="0" name="value[]" />' +
                                            '</div>';
        }

        function addOverseas(){
            document.getElementById('addOverseas').innerHTML += 
                                            '<input type="hidden" name="category[]" value="Luar Negeri">' +
                                            '<div class="col-md-6">' +
                                                '<p>Lokasi</p>' +
                                                '<input class="form-control" type="text" name="location[]" />' +
                                            '</div>' +
                                            '<div class="col-md-6">' +
                                                '<p>Nilai</p>' +
                                                '<input class="form-control" type="number" value="0" name="value[]" />' +
                                            '</div>';
        }

        function addECommerce(){
            document.getElementById('addECommerce').innerHTML += 
                                            '<input type="hidden" name="category[]" value="E-Commerce">' +
                                            '<div class="col-md-6">' +
                                                '<p>Lokasi</p>' +
                                                '<input class="form-control" type="text" name="location[]" />' +
                                            '</div>' +
                                            '<div class="col-md-6">' +
                                                '<p>Nilai</p>' +
                                                '<input class="form-control" type="number" value="0" name="value[]" />' +
                                            '</div>';
        }
        
    </script>
@endpush