@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-9">
                        <h3>Riwayat Fasilitas</h3>
                    </div>
                </div>
                @include('layouts.alert')
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Jenis Fasilitas</th>
                                    <th>Detail Fasilitas</th>
                                    <th>Jenis Produk</th>
                                    <th>Waktu</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach (Auth::user()->Attendance as $index => $attendance)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$attendance->Facility->FacilityCategory->name}}</td>
                                        <td>{{$attendance->Facility->name}}</td>
                                        <td>Jenis Produk</td>
                                        <td>{{$attendance->Facility->start_date}} - {{$attendance->Facility->end_date}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
