@extends('layouts.app')

@section('content')
@if (!isset(Auth::user()->CategoryLevel5->title))
    <div class="alert alert-warning" role="alert">
    Data Anda belum lengkap, silahkan lengkapi pada menu <b><a href="{{route('user.edit')}}" style="color: white">Ubah Profil</a></b>
    </div>
@endif
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <form action="{{route('dashboard.year')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-2">
                            <label for="">Pilih Tahun: </label>
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" type="text" name="year" id="datepicker" />
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary full-width">Pilih</button>
                        </div>
                    </div>
                </form>
                @include('layouts.alert')
            </div>
        </div>
    </div>
</div>
@isset ($data)
    <div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <h3>Data IKM Tahun {{$data->year}}</h3>
                <form action="">
                    <div class="form-group">
                        <label for="">Tahun</label>
                        <input class="form-control" type="number" value="{{$data->year}}" disabled>
                    </div>
                    
                    <div class="form-group">
                        <label for="">Investasi</label>
                        <input class="form-control" type="text" value="{{number_format($data->investment)}}" disabled/>
                    </div>
                    
                    <hr>
                    <div class="form-group">
                        <b>Jenis Produk</b>
                        @foreach ($data->Product as $product)
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Nama</p>
                                    <input class="form-control" type="text" value="{{$product->name}}" disabled>
                                </div>
                                <div class="col-md-6">
                                    <p>Foto</p>
                                    <img class="img-thumbnail img-fluid" src="{{asset('storage/'.$product->photo)}}" alt="foto jenis produk">
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Kapasitas Produksi</label>
                                        <input class="form-control" type="text" style="text-transform: uppercase" value="{{$product->production_capacity}}" disabled />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Nilai Produksi Tahunan</label>
                                        <input class="form-control" type="text" value="{{number_format($product->production_value)}}" disabled />
                                    </div>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                    <hr>
                    <div class="form-group">
                        <b>Sumber Bahan Baku</b>
                        @foreach ($data->Material as $material)
                            <div class="row">
                                <div class="col-md-4">
                                    <p>Asal</p>
                                    <input class="form-control" type="text" value="{{$material->origin}}" disabled>
                                </div>
                                <div class="col-md-4">
                                    <p>Jenis</p>
                                    <input class="form-control" type="text" value="{{$material->kind}}" disabled>
                                </div>
                                <div class="col-md-4">
                                    <p>Jumlah</p>
                                    <input class="form-control" type="number" value="{{number_format($material->quantity)}}" disabled>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <hr>
                    <div class="form-group">
                        <b>Jumlah Tenaga Kerja</b>
                        <div class="row">
                            <div class="col-md-6">
                                <p>WNI Pria</p>
                                <input class="form-control" type="number" value="{{$data->wni_male}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <p>WNI Wanita</p>
                                <input class="form-control" type="number" value="{{$data->wni_female}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <p>WNA Pria</p>
                                <input class="form-control" type="number" value="{{$data->wna_male}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <p>WNA Wanita</p>
                                <input class="form-control" type="number" value="{{$data->wna_female}}" disabled />
                            </div>
                            <div class="col-md-12">
                                <p>Total Tenaga Kerja</p>
                                <input class="form-control" type="number" value="{{$data->labor_total}}" disabled />
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <b>Jenis Perizinan</b>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Izin Usaha Industri</label>
                            </div>
                            <div class="col-md-6">
                                <p>Nomor</p>
                                <input class="form-control" type="text" value="{{$data->industrial_permit_number}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <p>Masa Berlaku</p>
                                <input class="form-control" type="text" value="{{$data->industrial_permit_period}}" disabled/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">SIUP</label>
                            </div>
                            <div class="col-md-6">
                                <p>Nomor</p>
                                <input class="form-control" type="text" value="{{$data->siup_number}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <p>Masa Berlaku</p>
                                <input class="form-control" type="text" value="{{$data->siup_period}}" disabled/>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">TDP</label>
                            </div>
                            <div class="col-md-6">
                                <p>Nomor</p>
                                <input class="form-control" type="text" value="{{$data->tdp_number}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <p>Masa Berlaku</p>
                                <input class="form-control" type="text" value="{{$data->tdp_period}}" disabled/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">NIB</label>
                            </div>
                            <div class="col-md-6">
                                <p>Nomor</p>
                                <input class="form-control" type="text" value="{{$data->nib_number}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <p>Masa Berlaku</p>
                                <input class="form-control" type="text" value="{{$data->nib_period}}" disabled/>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <b>Jenis Standarisasi</b>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">PIRT</label>
                            </div>
                            <div class="col-md-6">
                                <p>Nomor</p>
                                <input class="form-control" type="text" value="{{$data->pirt_number}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <p>Masa Berlaku</p>
                                <input class="form-control" type="text" value="{{$data->pirt_period}}" disabled/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">SNI</label>
                            </div>
                            <div class="col-md-6">
                                <p>Nomor</p>
                                <input class="form-control" type="text" value="{{$data->sni_number}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <p>Masa Berlaku</p>
                                <input class="form-control" type="text" value="{{$data->sni_period}}" disabled/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">ISO</label>
                            </div>
                            <div class="col-md-6">
                                <p>Nomor</p>
                                <input class="form-control" type="text" value="{{$data->iso_number}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <p>Masa Berlaku</p>
                                <input class="form-control" type="text" value="{{$data->iso_period}}" disabled/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Halal</label>
                            </div>
                            <div class="col-md-6">
                                <p>Nomor</p>
                                <input class="form-control" type="text" value="{{$data->halal_number}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <p>Masa Berlaku</p>
                                <input class="form-control" type="text" value="{{$data->halal_period}}" disabled/>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <b>Segmen Pemasaran</b>
                        @foreach ($data->Marketing as $marketing)
                            <div class="row">
                                <div class="col-md-3">
                                    <p>Kategori</p>
                                    <input class="form-control" type="text" value="{{$marketing->category}}" disabled />
                                </div>
                                <div class="col-md-3">
                                    <p>Lokasi</p>
                                    <input class="form-control" type="text" value="{{$marketing->location}}" disabled />
                                </div>
                                <div class="col-md-3">
                                    <p>Nilai</p>
                                    <input class="form-control" type="text" value="{{number_format($marketing->value)}}" disabled />
                                </div>
                                <div class="col-md-3">
                                    <p>Keterangan</p>
                                    <input class="form-control" type="text" value="{{$marketing->description}}" disabled />
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <b>Fasilitas</b>
                        <div class="row">
                            <div class="col-md-12">
                                <input class="form-control" type="text" value="{{$data->Facility->name}}" disabled />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endisset
@endsection
@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
    <script>
        $("#datepicker").datepicker({
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years"
        });
    </script>
@endpush