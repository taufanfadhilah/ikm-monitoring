@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                @include('layouts.alert')
                <h3>Ubah Bidang / UPTD Pembina</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('category.update', ['category' => $category->id])}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input class="form-control" type="text" name="name" value="{{$category->name}}" required/>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary full-width">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection