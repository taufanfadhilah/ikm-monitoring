@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-9">
                        <h3>Bidang / UPTD Pembina</h3>
                    </div>
                    <div class="col-md-3">
                        <a href="{{route('category.create')}}" class="btn btn-success full-width">Tambah Bidang / UPTD</a>
                    </div>
                </div>
                @include('layouts.alert')
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th colspan="2">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $index => $category)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$category->name}}</td>
                                        <td>
                                            <a href="{{route('category.edit', ['category' => $category->id])}}" class="btn btn-warning">Ubah</a>
                                        </td>
                                        <td>
                                            <form action="{{route('category.destroy', ['category' => $category->id])}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
