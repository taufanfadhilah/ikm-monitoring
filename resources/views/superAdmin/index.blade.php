@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <h3>Grafik Fasilitas di Jawa Barat</h3>
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="myChart" width="400" height="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <h3>Data Fasilitas di Jawa Barat</h3>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No. </th>
                                    <th>Kota / Kabupaten</th>
                                    <th>Jumlah</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($districts as $index => $district)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$district->name}}</td>
                                        <td>{{count($district->Attendance)}}</td>
                                        <td><a href="{{route('superAdmin.detailChart', ['district' => $district->id])}}">Detail</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ]
            } );
        } );

        var ctx = document.getElementById('myChart');
        var districts = {!! json_encode($districts) !!};
        function random_rgba() {
            var o = Math.round, r = Math.random, s = 255;
            return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ', 0.2)';
        }
        
        var districtNames = new Array()
        var districtAttendances = new Array()
        var districtColors = new Array()
        districts.forEach(district => {
            districtNames.push(district.name)
            districtAttendances.push(district.attendance.length)
            districtColors.push(random_rgba())
        });

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: districtNames,
                datasets: [{
                    label: 'peserta',
                    data: districtAttendances,
                    backgroundColor: districtColors,
                    borderColor: districtColors,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endpush