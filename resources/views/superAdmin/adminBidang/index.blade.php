@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-9">
                        <h3>Daftar Admin Bidang</h3>
                    </div>
                    <div class="col-md-3">
                        <a href="{{route('superAdmin.adminBidang.create')}}" class="btn btn-success full-width">Tambah Admin Bidang</a>
                    </div>
                </div>
                @include('layouts.alert')
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Waktu Dibuat</th>
                                    <th>Waktu Dihapus</th>
                                    <th colspan="2">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $index => $user)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->created_at->format('d M Y')}}</td>
                                        <td>
                                            @if ($user->deleted_at)
                                                {{$user->deleted_at->format('d M Y')}}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('superAdmin.adminBidang.edit', ['user' => $user->id])}}" class="btn btn-warning">Ubah</a>
                                        </td>
                                        @if (!$user->deleted_at)
                                            <td>
                                                <form action="{{route('superAdmin.adminBidang.destroy', ['user' => $user->id])}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                                </form>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
