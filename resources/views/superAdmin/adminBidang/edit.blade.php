@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                @include('layouts.alert')
                <h3>Ubah Admin Bidang</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('superAdmin.adminBidang.update', ['user' => $user->id])}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input class="form-control" type="text" name="name" value="{{$user->name}}" required/>
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input class="form-control" type="text" name="email" value="{{$user->email}}" required/>
                            </div>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input class="form-control" type="password" name="password"/>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary full-width">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection