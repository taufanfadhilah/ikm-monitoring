@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                @include('layouts.alert')
                <h3>Ubah Jenis Fasilitas</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('superAdmin.facilityCategory.update', ['facilityCategory' => $facilityCategory->id])}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input class="form-control" type="text" name="name" value="{{$facilityCategory->name}}" required/>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary full-width">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection