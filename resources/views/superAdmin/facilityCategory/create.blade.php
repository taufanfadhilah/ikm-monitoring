@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-block">
                @include('layouts.alert')
                <h3>Tambah Jenis Fasilitas</h3>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{route('superAdmin.facilityCategory.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input class="form-control" type="text" name="name" required/>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary full-width">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection